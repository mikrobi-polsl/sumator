# SUMATOR LICZB CZTEROBITOWYCH


## Opis:

Celem projektu jest stworzenie Sumatora liczb czterobitowych z użyciem elementów dyskretnych, przewlekanych: tranzystorów bipolarnych, rezystorów, diod elektroluminescencyjnych oraz przełączników dwupozycyjnych.


## Spis elementów:

Nazwa:							Sztuk:
- [ ] Rezystor 150				13
- [ ] Rezystor 10k				7
- [ ] Rezystor 100k				89
- [ ] Tranzystor NPN BC547		43
- [ ] Tranzystor PNP BC557		22
- [ ] Dioda LED					13
- [ ] Przełącznik dwupozycyjny	9
